<?php

use App\Http\Controllers\MarvelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MarvelController::class, 'index'])->name('marvel.index');
Route::get('/comic/{id}', [MarvelController::class, 'comic'])->name('marvel.comic');

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Http::macro('marvel', function () {
            return Http::baseUrl(env('MARVEL_API_URL'));
        });

        Http::macro('supabase', function () {
            return Http::withHeaders([
                'apikey' => env('SUPABASE_PUBLIC_KEY'),
                'Authorization' => 'Bearer '.env('SUPABASE_PUBLIC_KEY'),
            ])
            ->baseUrl(\env('SUPABASE_API_URL'));
        });
    }
}

<?php

namespace Tests\Unit;

use App\Helpers\Helper;
use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testMarvelQueryParamsSuccess()
    {
        $this->assertCount(3, Helper::marvelQueryParams(), 'The function returns an array with 3 elements by default.');
        $this->assertCount(5, Helper::marvelQueryParams([
            'limit' => 15,
            'offset' => 10,
        ]));
    }
}

<?php

namespace App\Helpers;

final class Helper
{
    /**
     * Returns the default query parameters plus the optional parameters to query the Marvel API.
     *
     * @param array $params array<string, mixed>
     * @return array array<string, mixed>
     */
    public static function marvelQueryParams(array $params = []): array
    {
        $timestamp = \time();
        $credentials = [
            'apikey' => env('MARVEL_PUBLIC_KEY'),
            'hash' => \md5($timestamp.env('MARVEL_PRIVATE_KEY').env('MARVEL_PUBLIC_KEY')),
            'ts' => $timestamp,
        ];

        if ([] === $params) {
            return $credentials;
        }

        return $credentials + $params;
    }
}

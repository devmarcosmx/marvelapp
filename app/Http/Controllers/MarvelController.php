<?php

namespace App\Http\Controllers;

use App\Http\Resources\MarvelResource;
use App\Http\Resources\SupabaseResource;
use Illuminate\Contracts\View\View;

class MarvelController extends Controller
{
    public function index(): View
    {
        return view('sections.comics', [
            'comics' => (new MarvelResource())->getCommics(),
        ]);
    }

    public function comic(int $id): View
    {
        return view('sections.comic', [
            'comic' => (new MarvelResource())->getComicById($id),
            'comments' => (new SupabaseResource())->getCommentsByCommic($id),
        ]);
    }
}

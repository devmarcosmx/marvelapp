<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Http;

final class SupabaseResource
{
    public function getCommentsByCommic(int $id): array
    {
        return Http::supabase()
        ->get('/rest/v1/comments', [
            'select' => '*',
            'comic_id' => 'eq.'.$id,
        ])
        ->throw()
        ->object();
    }
}
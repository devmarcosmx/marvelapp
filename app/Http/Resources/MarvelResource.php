<?php

namespace App\Http\Resources;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Http;

final class MarvelResource
{
    public function getCommics(): array
    {
        return Http::marvel()
        ->get('/v1/public/comics', Helper::marvelQueryParams([
            'limit' => 6,
            'offset' => 100,
        ]))
        ->throw()
        ->object()
        ->data
        ->results;
    }

    public function getComicById(int $id): \stdClass
    {
        return Http::marvel()
        ->get('/v1/public/comics/'.$id, Helper::marvelQueryParams())
        ->throw()
        ->object()
        ->data
        ->results[0];
    }
}